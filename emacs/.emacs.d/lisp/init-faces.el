;; init-faces.el --- Faces definitons

(custom-set-faces
 '(lsp-ui-sideline-code-action ((t (:foreground "#ffba27")))))

(provide 'init-faces)
